<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe e.V. <contact@fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Container Utilities

[![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/container-utils/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/container-utils)
[![Coverage](https://git.fsfe.org/fsfe-system-hackers/container-utils/raw/branch/master/coverage.svg)](https://git.fsfe.org/fsfe-system-hackers/container-utils)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/container-utils)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/container-utils)

The goal of this CLI tool is to simplify the regular analysis and maintenance of
container deployments with Gitea and Drone.

## Usage

There are two environment variables that you need to set in order to ensure
proper operation of the tool. These are `DRONE_TOKEN` and `GITEA_TOKEN`. If you
use [`direnv`](https://direnv.net/), simply create a file called `.envrc` in
the repository with the following content:

```bash
export DRONE_TOKEN=***
export GITEA_TOKEN=***
```

Next, install the module and its dependencies by running `poetry install`. You
can then access the command `cu` by running `poetry run cu`. If you do that you
are presented with a help page that looks like this:

```
Usage: cu [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  analyse
  rebuild
```

### Analysis

The `analyse` subcommand can be used as follows:

```
Usage: cu analyse [OPTIONS]

Options:
  -t, --topic TEXT  Specify topics (e.g. -t 'cont1-noris' -t cont2-noris)
                    [default: docker]
  --help            Show this message and exit.
```

### Rebuilding

The `rebuild` subcommand can be used as follows:

```
Usage: cu rebuild [OPTIONS]

Options:
  -a, --all-repos
  -s, --slug TEXT    Specify repository slug (e.g. 'linus/dotfiles').
  -b, --branch TEXT  Specify repository target branch.  [default: default
                     branch of repository]
  -e, --event TEXT   Specify the event.  [default: push]
  -y, --no-confirm   Don't ask for confirmation.
  --help             Show this message and exit.
```
