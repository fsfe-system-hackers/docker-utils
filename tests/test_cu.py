#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe e.V. <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import time

from click.testing import CliRunner

from container_utils.drone import drone_last_build_number
from container_utils.cu import analyse, rebuild

runner = CliRunner()


def test_analyse():
    response = runner.invoke(analyse, ["-t" "cont1-noris"])
    assert response.exit_code == 0
    assert "Image Statistics" in response.output
    assert "bitnami/postgresql:13" in response.output


def test_rebuild_default_branch_noconfirm():
    repo_owner = "fsfe-system-hackers"
    repo_name = "minimal-docker"
    slug = "/".join([repo_owner, repo_name])
    cur_build_number = drone_last_build_number(repo_owner, repo_name)
    response = runner.invoke(rebuild, ["-s", slug, "-y"])
    assert response.exit_code == 0
    new_build_number = drone_last_build_number(repo_owner, repo_name)
    assert new_build_number > cur_build_number


def test_rebuild_specified_branch_noconfirm():
    repo_owner = "fsfe-system-hackers"
    repo_name = "minimal-docker"
    slug = "/".join([repo_owner, repo_name])
    cur_build_number = drone_last_build_number(repo_owner, repo_name)
    response = runner.invoke(rebuild, ["-s", slug, "-b", "main", "-y"])
    assert response.exit_code == 0
    new_build_number = drone_last_build_number(repo_owner, repo_name)
    assert new_build_number > cur_build_number


def test_rebuild_confirm_y():
    repo_owner = "fsfe-system-hackers"
    repo_name = "minimal-docker"
    slug = "/".join([repo_owner, repo_name])
    cur_build_number = drone_last_build_number(repo_owner, repo_name)
    response = runner.invoke(rebuild, ["-s", slug], input="y")
    assert response.exit_code == 0
    new_build_number = drone_last_build_number(repo_owner, repo_name)
    assert new_build_number > cur_build_number


def test_rebuild_confirm_n():
    repo_owner = "fsfe-system-hackers"
    repo_name = "minimal-docker"
    slug = "/".join([repo_owner, repo_name])
    cur_build_number = drone_last_build_number(repo_owner, repo_name)
    response = runner.invoke(rebuild, ["-s", slug], input="n")
    assert response.exit_code == 0
    new_build_number = drone_last_build_number(repo_owner, repo_name)
    assert new_build_number == cur_build_number


def test_correct_exit():
    repo_owner = "fsfe-system-hackers"
    repo_name = "minimal-docker"
    slug = "/".join([repo_owner, repo_name])
    response = runner.invoke(rebuild, ["-a", "-s", slug], input="y")
    assert response.exit_code == 1
