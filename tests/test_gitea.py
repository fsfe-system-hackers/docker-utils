#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe e.V. <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import re
import giteapy

from container_utils.gitea import (
    gitea_repos_for_user,
    gitea_repos_for_org,
    gitea_repos_with_topics,
    gitea_repo,
    gitea_repo_topics,
    gitea_repo_latest_commit,
    gitea_repo_tree,
    gitea_repo_raw_file,
    gitea_dockerfile_images,
    gitea_yamlfile_images,
    gitea_images,
    gitea_repos_with_image,
)


def test_gitea_repos_for_user():
    repos = gitea_repos_for_user()
    assert type(repos) == list
    assert len(repos) > 15


def test_gitea_repos_for_org():
    repos = gitea_repos_for_org("fsfe-system-hackers")
    assert type(repos) == list
    assert len(repos) > 15


def test_gitea_repos_with_topics():
    repos = gitea_repos_with_topics(["docker"])
    assert len(repos) > 15


def test_gitea_repo():
    repo = gitea_repo("fsfe-system-hackers", "lychee")
    assert type(repo) == giteapy.models.repository.Repository
    assert repo.name == "lychee"
    assert repo.default_branch == "master"


def test_gitea_repo_topics():
    topics = gitea_repo_topics("fsfe-system-hackers", "lychee")
    assert type(topics) == list
    assert len(topics) >= 1
    assert "docker" in topics


def test_gitea_repo_latest_commit():
    sha = gitea_repo_latest_commit("fsfe-system-hackers", "inventory", "dynamic-bash")
    assert sha == "f2ef450ae0ec93c445033c4c552633d23ad418e9"


def test_gitea_repo_tree():
    tree = gitea_repo_tree(
        "fsfe-system-hackers", "inventory", "f2ef450ae0ec93c445033c4c552633d23ad418e9"
    )
    assert "hosts.json" in [item.path for item in tree]


def test_gitea_repo_get_raw_file():
    file = gitea_repo_raw_file(
        "fsfe-system-hackers",
        "forms",
        ".gitignore",
        "f76283f6406ac5efbfc4446c9737362cafa3ab4a",
    )
    assert len(re.findall("htmlcov", file.decode())) == 1


def test_gitea_dockerfile_images():
    images = gitea_dockerfile_images(
        "fsfe-system-hackers", "forms", "f76283f6406ac5efbfc4446c9737362cafa3ab4a"
    )
    assert type(images) == list
    assert len(images) == 3
    assert "fsfe/alpine-pipenv:latest" in images


def test_gitea_yamlfile_images():
    images = gitea_yamlfile_images(
        "fsfe-system-hackers", "forms", "f76283f6406ac5efbfc4446c9737362cafa3ab4a"
    )
    assert type(images) == list
    assert len(images) > 5
    assert any("redis:6" in image for image in images)


def test_gitea_images():
    images = gitea_images(
        "fsfe-system-hackers", "forms", "f76283f6406ac5efbfc4446c9737362cafa3ab4a"
    )
    assert type(images) == list
    assert len(images) >= 7
    assert any("redis:6" in image for image in images)


def test_gitea_repos_with_image():
    repos = gitea_repos_with_image("fsfe/reuse:latest")
    assert "fsfe-system-hackers/forms" in repos
