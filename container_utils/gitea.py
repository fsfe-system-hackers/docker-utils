#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe e.V. <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import re
from itertools import chain
from typing import Sequence

import giteapy
import requests
import yaml
from giteapy.rest import ApiException
from nested_lookup import nested_lookup
from container_utils.console import console

# Configure giteapy
api_config = giteapy.Configuration()
api_config.host = os.environ.get("GITEA_SERVER", "https://git.fsfe.org") + "/api/v1"
api_config.api_key_prefix["Authorization"] = "token"

try:
    api_config.api_key["Authorization"] = os.environ.get("GITEA_TOKEN")
    API_URL = os.environ.get("GITEA_SERVER", "https://git.fsfe.org") + "/api/v1"
    AUTH = {"Authorization": "Bearer " + str(os.getenv("GITEA_TOKEN"))}
except KeyError as e:  # pragma: no cover
    console.log("No Gitea token found in environment variable. Plase check.")
    raise e


# Create giteapy API classes
user_api = giteapy.UserApi(giteapy.ApiClient(api_config))
orgs_api = giteapy.OrganizationApi(giteapy.ApiClient(api_config))
repo_api = giteapy.RepositoryApi(giteapy.ApiClient(api_config))


def gitea_repos_for_user() -> Sequence[list]:
    """Get a list of repos for the currently authenticated user."""
    try:
        response = user_api.user_current_list_repos()
        return response
    except ApiException as e:  # pragma: no cover
        console.log("Exception when calling gitea_repos_for_user(): %s\n" % e)
        raise e


def gitea_repos_for_org(org_name: str) -> Sequence[list]:
    """Get a list of repos for an organization."""
    try:
        response = orgs_api.org_list_repos(org_name)
        return response
    except ApiException as e:  # pragma: no cover
        console.log("Exception when calling gitea_repos_for_org: %s\n" % e)
        raise e


def gitea_repos_with_topics(topics: list) -> Sequence[list]:
    """Get a list of active repos with all specified topics for current user."""
    all_repos = gitea_repos_for_user()
    repos_with_topics = []
    for repo in all_repos:
        if repo.archived == True:
            continue
        owner_name, repo_name = repo.full_name.split("/")
        repo_topics = gitea_repo_topics(owner_name, repo_name)
        # If all topics are in repo_topics
        if all(r in repo_topics for r in topics):
            repos_with_topics.append(repo)
    return repos_with_topics


def gitea_repo(owner_name: str, repo_name: str) -> giteapy.models.repository.Repository:
    """Get data of specific repository."""
    try:
        response = repo_api.repo_get(owner=owner_name, repo=repo_name)
        return response
    except ApiException as e:  # pragma: no cover
        console.log("Exception when calling gitea_repo(): %s\n." % e)
        raise e


def gitea_repo_topics(owner_name: str, repo_name: str) -> Sequence[list]:
    """Get a list of topics for an owner and a repo."""
    try:
        topics = repo_api.repo_list_topics(owner_name, repo_name).topics
        return topics
    except ApiException as e:  # pragma: no cover
        console.log("Exception when calling gitea_repo_topics: %s\n" % e)
        raise e


def gitea_repo_latest_commit(
    owner_name: str, repo_name: str, branch: str = "master"
) -> str:
    """Get sha of latest commit to repo on specified branch."""
    try:
        response = repo_api.repo_get_all_commits(owner_name, repo_name, sha=branch)
        return response[0].sha
    except ApiException as e:  # pragma: no cover
        console.log("Exception when calling gitea_repo_latest_commit(): %s\n" % e)
        raise e


def gitea_repo_tree(owner_name: str, repo_name: str, sha: str) -> Sequence[list]:
    """Get a tree of an repo for a commit."""
    try:
        response = repo_api.get_tree(owner_name, repo_name, sha, recursive=True)
        return response.tree
    except ApiException as e:  # pragma: no cover
        console.log("Exception when calling gitea_repo_tree(): %s\n" % e)
        raise e


def gitea_repo_raw_file(
    owner_name: str, repo_name: str, filepath: str, ref: str = "master"
) -> bytes:
    """Get a file as bytes from repo at filepath for reference."""
    try:
        url = f"{API_URL}/repos/{owner_name}/{repo_name}/raw/{filepath}?ref={ref}"
        response = requests.get(url, headers=AUTH).content
        return response
    except ApiException as e:  # pragma: no cover
        console.log("Exception when calling gitea_repo_raw_file(): %s\n" % e)
        raise e


def gitea_dockerfile_images(
    owner_name: str, repo_name: str, branch: str, sha: str = "latest"
) -> Sequence[list]:
    """Get list of images used in Dockerfiles of repo."""
    if sha == "latest":
        sha = gitea_repo_latest_commit(owner_name, repo_name, branch=branch)

    trees = gitea_repo_tree(owner_name, repo_name, sha)

    images = []
    dockerfiles = [i.path for i in trees if "Docker" in i.path]
    for dockerfile in dockerfiles:
        content = gitea_repo_raw_file(owner_name, repo_name, dockerfile, ref=branch)
        image = re.search(r"FROM\s+([/:.\w-]+)", str(content)).groups()[0]
        images.append(image)
    return images


def gitea_yamlfile_images(
    owner_name: str, repo_name: str, branch: str, sha: str = "latest"
) -> Sequence[list]:
    """Get list of images used in files ending in .yml or .yaml."""

    if sha == "latest":
        sha = gitea_repo_latest_commit(owner_name, repo_name, branch=branch)

    trees = gitea_repo_tree(owner_name, repo_name, sha)

    images = []

    # Find locally built images first
    yamlfiles_for_local = [
        i.path for i in trees if i.path.endswith(".yml") or i.path.endswith(".yaml")
    ]
    for yamlfile in yamlfiles_for_local:
        file_content = gitea_repo_raw_file(owner_name, repo_name, yamlfile, sha)
        parsed = yaml.load_all(file_content, Loader=yaml.BaseLoader)

        for file in parsed:
            for service in nested_lookup("services", file):
                for key in service.keys():
                    for section in nested_lookup(key, service):
                        image = nested_lookup("image", section)
                        if nested_lookup("build", section):
                            try:
                                images.append(f"{image[0]} (locally built)")
                            except IndexError:
                                images.append(f"{key} (locally built)")
                        else:
                            continue

    # Find all others
    yamlfiles_for_all = [
        i.path for i in trees if i.path.endswith(".yml") or i.path.endswith(".yaml")
    ]
    for yamlfile in yamlfiles_for_all:
        file_content = gitea_repo_raw_file(owner_name, repo_name, yamlfile)
        parsed = yaml.load_all(file_content, Loader=yaml.BaseLoader)

        for file in parsed:
            images_in_yamlfile = nested_lookup("image", file)
            if images_in_yamlfile:
                for image in images_in_yamlfile:
                    if any(f"{image}" in img for img in images):
                        continue
                    else:
                        images.append(image)

    return images


def gitea_images(owner_name: str, repo_name: str, branch: str, sha: str = "latest"):
    """Get all images from a repo."""
    if sha == "latest":
        sha = gitea_repo_latest_commit(owner_name, repo_name, branch=branch)

    images = []
    images.append(gitea_dockerfile_images(owner_name, repo_name, branch, sha))
    images.append(gitea_yamlfile_images(owner_name, repo_name, branch, sha))

    return list(chain(*images))


def gitea_repos_with_image(image_name: str) -> Sequence[list]:
    """Get a list of repos in which an image is used."""
    all_docker_repos = gitea_repos_with_topics(["docker"])
    repo_image_dict = {}
    for repo in all_docker_repos:
        slug = repo.full_name
        owner_name, repo_name = slug.split("/")
        branch = repo.default_branch
        with console.status(
            f"Extracting image data from {slug}. Please be patient..."
        ) as status:
            images = gitea_images(owner_name, repo_name, branch, "latest")
        if not images:
            console.log("No image data in {0}.".format(slug))
        # Assign the image list (value) to the repo slug (key)
        repo_image_dict[slug] = images
    repos_with_image = []
    for slug, images in repo_image_dict.items():
        if image_name in images:
            repos_with_image.append(slug)
    return repos_with_image
