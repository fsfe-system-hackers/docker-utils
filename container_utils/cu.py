#!/usr/bin/env python3

# Copyright © 2022 Free Software Foundation Europe
# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe e.V. <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import sys
import time

import click
from collections import Counter

# Import API functions
from container_utils.drone import (
    BASE_URL,
    drone_restart_build,
    drone_sync,
    drone_active_repos,
    drone_last_build_number,
)
from container_utils.gitea import gitea_repo, gitea_repos_with_topics, gitea_images
from container_utils.console import console

from rich.table import Table


@click.group()
def cli():
    pass  # pragma: no cover


@cli.command()
@click.option(
    "-t",
    "--topic",
    default=["docker"],
    show_default=True,
    multiple=True,
    help="Specify topics (e.g. -t 'cont1-noris' -t cont2-noris)",
)
def analyse(topic):
    console.log("Started the analysis.")

    # Construct dictionary where the keys are repo slugs and the values are images
    repo_image_dict = {}
    for repo in gitea_repos_with_topics(list(topic)):
        with console.status(f"Extracting image data from {repo.full_name}") as status:
            slug = repo.full_name
            owner_name, repo_name = slug.split("/")
            branch = repo.default_branch
            images = gitea_images(owner_name, repo_name, branch)
            if not images:  # pragma: no cover
                console.log(f"No images found in {repo.full_name}.")
            repo_image_dict[slug] = images

    # Construct dictionary where the keys are image names and the values are image tags
    image_tag_dict = {}
    for repo in repo_image_dict:
        for image in repo_image_dict[repo]:
            try:
                image_name, image_tag = image.split(":")
                # Exclude locally built images
                if not "locally built" in image_tag:
                    image_tag_dict.setdefault(image_name, []).append(image_tag)
            except ValueError:
                continue

    # Output information on duplicate image tags
    multiple_tags = False
    print()
    console.rule("Duplicate Image Analysis", align="left")
    for image, tags in sorted(image_tag_dict.items()):
        tags = sorted(tags)
        if len(set(tags)) > 1:  # pragma: no cover
            multiple_tags = True
            console.print(
                f"\n[red]WARNING[/red] There are different versions of [blue]{image}[/blue]"
            )
            for duplicate_version in set(sorted(tags)):
                duplicate_image_version = [image, duplicate_version]
                console.print("[blue]{}:{}[/blue] in:".format(*duplicate_image_version))
                for repo, version in repo_image_dict.items():
                    if ":".join(duplicate_image_version) in version:
                        console.print(f"- [green]https://git.fsfe.org/{repo}[/green]")
    if not multiple_tags:
        console.log(
            "[green]✓[/green] No multiple tags were detected in the scraped repos."
        )

    # Construct dictionary where the keys are image names and the values are unique image tags
    image_tag_unique_dict = {}
    for image_name, image_tags in image_tag_dict.items():
        image_tag_unique_dict[image_name] = list(set(image_tags))

    # Output statistics about images in scraped repos
    print()
    console.rule("Image Statistics", align="left")
    print()
    # Create a list of all used images (with tags)
    images = [
        ":".join([image, tag]) for image, tags in image_tag_dict.items() for tag in tags
    ]
    unique_images = sorted(set(images))
    console.print(f"There are {len(unique_images)} distinct third-party images in use.")
    console.print(f"In total, there are {len(images)} third-party images.")
    table = Table(show_header=True, header_style="blue")
    table.add_column("Image (name:tag)", width=50)
    table.add_column("Count")
    image_count = dict(Counter(images).most_common())
    for image, count in image_count.items():
        table.add_row(image, str(count))
    console.print(table)


@cli.command(no_args_is_help=True)
@click.option("-a", "--all-repos", default=False, is_flag=True)
@click.option(
    "-s",
    "--slug",
    type=str,
    help="Specify repository slug (e.g. 'linus/dotfiles').",
)
@click.option(
    "-b",
    "--branch",
    default="default",
    type=str,
    help="Specify repository target branch.  [default: default branch of repository]",
)
@click.option(
    "-e",
    "--event",
    type=str,
    default="push",
    show_default=True,
    help="Specify the event.",
)
@click.option(
    "-y",
    "--no-confirm",
    default=False,
    is_flag=True,
    help="Don't ask for confirmation.",
)
def rebuild(all_repos, slug, branch, event, no_confirm):
    # Handle all repos
    if all_repos and not slug:  # pragma: no cover
        console.log("Started rebuilding script for all repos.")
        drone_sync()
        for repo in drone_active_repos():
            slug = repo["slug"]
            owner_name, repo_name = slug.split("/")
            branch = repo["default_branch"]
            event = "push"

            # Cover special FSFE cases
            # 1) fsfe-system-hackers/fsfe-cd
            if repo_name == "fsfe-cd":
                branch = "production"
            # 2) pmpc/website
            if slug == "pmpc/website":
                event = "cron"

            build_number = drone_last_build_number(owner_name, repo_name, [branch])
            if not no_confirm:
                if click.confirm(
                    f"Would you like to rebuild {BASE_URL}/{slug}/{build_number}?",
                    default=True,
                ):
                    drone_restart_build(owner_name, repo_name, build_number)
                    console.log(
                        f"[green]✓[/green] Rebuild triggered! Check progress: {BASE_URL}/{slug}/{build_number + 1}"
                    )
                else:
                    console.log("OK. Continuing with next repo.")
            else:
                drone_restart_build(owner_name, repo_name, build_number)
                console.log(
                    f"[green]✓[/green] Rebuild triggered! Check progress: {BASE_URL}/{slug}/{build_number + 1}"
                )
                time.sleep(3)

    # Handle just one selected repo
    elif slug and not all_repos:
        console.log(f"Started rebuilding script for a single repo.")
        owner_name, repo_name = slug.split("/")
        # Get default branch for repo
        if branch == "default":
            branch = gitea_repo(owner_name, repo_name).default_branch

        drone_sync()
        build_number = drone_last_build_number(owner_name, repo_name, [branch], event)

        if not no_confirm:
            if click.confirm(
                f"Would you like to rebuild {BASE_URL}/{slug}/{build_number}?",
                default=True,
            ):
                drone_restart_build(owner_name, repo_name, build_number)
                console.log(
                    f"[green]✓[/green] Rebuild triggered! Check progress: {BASE_URL}/{slug}/{build_number + 1}"
                )
            else:
                console.log("OK. Exiting.")
                sys.exit(0)
        else:
            drone_restart_build(owner_name, repo_name, build_number)
            console.log(
                f"[green]✓[/green] Rebuild triggered! Check progress: {BASE_URL}/{slug}/{build_number + 1}"
            )

    # In all other cases, exit the program
    else:
        console.log("This did not work. Please run 'cu --help' for more information.")
        sys.exit(1)


cli.add_command(analyse)
cli.add_command(rebuild)
